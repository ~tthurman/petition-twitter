#!/usr/bin/python

# petition-twitter.py
# Thomas Thurman, thomas@thurman.uk
# Public domain. Bug reports etc welcome.
#
# Very simple script to tweet the number of signatures
# to a petition on the UK government website.
# This should be run from a cronjob every five minutes
# (or however often you like).
# I apologise for the quick and dirty code:
# it had to be written in an hour.

import urllib
import json
import tweepy

################ begin config section

# URL of the petition.
PETITION_URL = 'https://petition.parliament.uk/petitions/131215'

# URL of the petition's JSON feed on the petitions site.
PETITION_DATA_URL = "https://petition.parliament.uk/petitions/131215.json"

# Name of a local file containing your Twitter credentials.
# It should contain a JSON object like this one:
# { 
# 	"consumer_key"        : "xxx",
# 	"consumer_secret"     : "xxx",
# 	"access_token"        : "xxx",
# 	"access_token_secret" : "xxx"
# }
# If you want to know how to get these values,
# I'll write it up somewhere.
TWITTER_CONFIG_FILE = "petition-twitter-config.json"

# Name of a file containing the number of signatures
# from last time we ran this script. If it doesn't
# exist, the script will create it.
PREvIOUS_CONFIG_FILE = "petition-twitter-previous.json"

################ end config section

def intWithCommas(x):
    """
    Format the integer "x" with commas, so
     1234567 -> "1,234,567"
    There are probably better ways of doing this.
    """
    if type(x) not in [type(0), type(0L)]:
        raise TypeError("Parameter must be an integer.")
    if x < 0:
        return '-' + intWithCommas(-x)
    result = ''

    while x >= 1000:
            x, r = divmod(x, 1000)
            result = ",%03d%s" % (r, result)
    return "%d%s" % (x, result)

def get_api(cfg):
    """
    Creates a tweepy object based on the configuration "cfg".
    """
    auth = tweepy.OAuthHandler(cfg['consumer_key'], cfg['consumer_secret'])
    auth.set_access_token(cfg['access_token'], cfg['access_token_secret'])
    return tweepy.API(auth)

def tweet(message):
    """
    Tweets the string "message".
    """
    cfg = json.load(open(TWITTER_CONFIG_FILE, "r"));
    api = get_api(cfg)
    status = api.update_status(status=message) 

def main():

    # Pick up the current number of signatures.

    jsondata = urllib.urlopen("https://petition.parliament.uk/petitions/131215.json").read()
    data = json.loads(jsondata)
    count = data['data']['attributes']['signature_count']

    # Format the message.

    number = intWithCommas(count)
    message = "Currently the EU second referendum petition has "+number+" signatures."

    # Add the previous number, if we have it.

    try:
        previous = json.load(open(PREVIOUS_CONFIG_FILE, 'r'))
    except:
        previous = {}

    if previous.has_key('count'):
        delta = count - previous['count']
        number = intWithCommas(delta)
        message += "\n\n(" + number+" more in the last five minutes.)"

    previous['count'] = count

    try:
        json.dump(previous, open(PREVIOUS_CONFIG_FILE, 'w'))
    except:
        # Can't write it. Never mind.
    	pass

    # Add a link to the petition site.

    message += '\n\n' + PETITION_URL

    # And tweet it!

    tweet(message)

if __name__=='__main__':
    main()

